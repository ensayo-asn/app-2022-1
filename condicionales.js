let x = '3';
let y = 3;

if (x == 3){
    console.log('Es igual a 3');
}

if (x === 3){
    console.log('Es igual a 3');
}

if (x == 3){
    console.log('son iguales');
}else{
    console.log('son diferentes');
}

if (x == 0){
    console.log('Es cero');
} else if(x == 1){
    console.log('Es uno');
} else{
    console.log('No es ninguno');
}

let dia = 10;
let nombre = '';
switch (dia) {
    case 1: nombre = 'Lunes';
        break;
    case 2: nombre = 'Martes';
        break;
    case 3: nombre = 'Miercoles';
        break;
    case 4: nombre = 'Jueves';
        break;
    default:
        console.log('No es ninguno');
        break;
}

if (x > 3 && x < 10){
    console.log('Es mayor a 3 y menos a 10');
}