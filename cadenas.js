let cadena1 = "Universidad"
console.log(cadena1);

let cadena2 = 'I don\'t know';
console.log(cadena2);

let cadena3 = "Universidad\nSanto\nTomas";
console.log(cadena3);

let cadena4 = "Star" + "Wars";
console.log(cadena4);
cadena4 += ":The return of the Yedi";
console.log(cadena4);

cadena4 = cadena4 + 1978;
console.log(cadena4);

//Convertir cadena en número
let cadena5 = "456";
let numero1 = parseInt(cadena5);
console.log(cadena5, typeof cadena5);
console.log(numero1, typeof numero1);

//Logitud de una cadena 
let longitud = cadena4.length;
console.log(longitud);

//Obtiene su caracter dada su cadena
let caracter = cadena4[0]; 
console.log(caracter);

let pelicula = "Star Wars";
let anio = 1978;
let director = "George Lucas";

let titulo1 = pelicula + '-' +  anio + " , Director: " + director
console.log(titulo1);
let titulo2 = `${pelicula} - ${anio}, director: ${director} - ${4 +5}`;
console.log(titulo2);

