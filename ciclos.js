//WHILE

let i = 1;
while (i <= 5) {
    console.log(i);
    //Instruccion de rompimiento
    i++;
}

let numero = 1;
while (numero <= 30){
    if (numero % 2 == 0){
        console.log(`${numero} es par`)
    }
    numero++;
}

let a = 10;
do {
    console.log(a)
    a++;
} while (a <= 5);

for (let x = 0; x < 100; x++) {
    console.log(x);
    x+= 10;           
}           

for (let x = 0, y = 5; x < 100 && y != 2; x+= 10, y--) {
    console.log(x, y);
}
