//Para imprimir un mesaje en consola
console.log("Hola mundo");

let X = 3; // Variables
var y = 4; // Variables

const j = 10; //Constante

//Tipos básicos
let entero = 20;
let real = 3.14;
let cadena = 'Catalina';
let booleanos = true; //True | false

//Para imprimir una variable en consola para
console.log(entero);

let tipo = typeof(entero);
console.log(tipo);