let x = 9; 

let usuario = {
    nombre: 'Lucy',
    apellidos: 'Smith',
    edad: 10,
    esAdulto: false,
    nombreCompleto(){
        return `${this.nombre} ${this.apellidos}`;
    },
};

console.log(usuario);
console.log(usuario['nombre']);
console.log(usuario.nombreCompleto());

//Modificar el objeto
usuario.apellidos = 'Willis';
usuario['nombre'] = 'Bruce';
console.log(usuario);

//Agragar un elemento al objeto
usuario.ciudad = 'Medallo';
console.log(usuario);

//Eliminar un atributo del objeto
delete usuario.ciudad;
console.log(usuario);

let tieneCoudad = usuario.hasOwnProperty('ciudad');
console.log(tieneCoudad);
let tieneNombre = usuario.hasOwnProperty('nombre');
console.log(tieneNombre);

let bandas = [
    {
        nombre: "AC/DC",
        pais: "Australia",
    },
    {
        nombre: "Guns and Roses",
        pais: "USA",
    },
    {
        nombre: "Queen",
        pais: "UK,"
    },
];

let hayGringos = bandas.some((banda) => banda.pais === "USA");
console.log(hayGringos);

usuario.notas = [3.4, 4.8, 1.5];
console.log(usuario);

usuario.ubicacion = {
    ciudad: "Medallo",
    departamento: "Antioquia",
    pais: "Colombia",
}

console.log(usuario);
console.log(usuario.ubicacion.departamento);
usuario.musica = bandas;
console.log(usuario);
console.log(usuario.musica[1].pais);