let numeros = [];
console.log(numeros);

let numeros2 = [10, 20 , 30];
console.log(numeros2);

let datos = [10, 3.2, "Cata", true];
console.log(datos);

console.log(datos[1]);
console.log(datos[6]);
console.log();

if (typeof datos[6] == "undefined") {
    console.log("No existe");
}

datos[2] = "Catalina";
console.log(datos);

let datos1 = [1, 2, 3];
let datos2 = datos1;
datos1[0] = 10;
console.log(datos1, datos2);

let num1 = [1, 2, 3];
let num2 = [...num1];
num1[0] = 10;
console.log(num1);
console.log(num2);

console.log("FUNCIONES")
let numbers = [10, 20, 30];
console.log(numbers);
//agregar al inicio 
numbers.unshift(40);
console.log(numbers);
//Agregar al final
numbers.push(20);
console.log(numbers);
//Eliminar el primer elemento 
numbers.shift();
console.log(numbers);
//Elimiar el ultimo elemento
numbers.pop();
console.log(numbers);
//Insertar elemento en una posicion especifica
numbers.splice(1, 0, 80);
console.log(numbers);
//Eliminar elemento en una posicion especifica
numbers.splice(1,1);
console.log(numbers);

let emojis = ["😊", "😁"];
console.log(emojis);

let nombre1 = ["Universidad"];
let nombre2 = ["Santo", "Tomas"];
let nombre3 = nombre1.concat(nombre2);
console.log(nombre3);




let x = 4;
console.log(typeof x);

x = "Cata";
console.log(typeof x);

x = true;
console.log(typeof x);

//FUNCIONES

let elements = [100, 200, 300, 400];
let nuevo1 = elements.map((e) =>{
    console.log("Transformando");
    let x = 0;
    x = e * 3;
    return(x);
});

let r1 = elements.reduce((x ,y) => x+ y);
console.log(r1)

let arreglo1 = [10, 20 , 50, 90];
console.log(arreglo1);

arreglo1.forEach(e => console.log(e));
arreglo1.forEach(e => {
    let x = e +3;
    console.log(x);
});

for(let e in arreglo1) {
    console.log(e);
}

let arreglo2 = [10, 3, 4, 6]
let todosPares = arreglo2.every((e) => e % 2 == 0);
console.log(todosPares);

let algunPar = arreglo2.some((e) => e % 2 != 0);
console.log(algunPar);

let notas = [2, 3, 1, 4, 5, 2, 3, 4];
let aprobados = notas.filter((nota) => nota >= 3);
console.log(aprobados);


