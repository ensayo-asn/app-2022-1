const fs = require('fs');
const path = require('path');

//Tiene la ruta del archivo de datos
const ruta = path.join(__dirname, "datos.json");

const cargarBandas = async ()=> {
    //Verifica que el archivo existe
    if (fs.existsSync(ruta)) {
        //Abrir el archivo
        let archivo = fs.readFileSync(ruta);
        //Lee la información del archivo    
        let datos = JSON.parse(archivo);
        //Retorna la info obtenida
        return datos;
    }
};

function mostrarBandas(datos) {
    for (const banda of datos.bandas) {
        console.log(`${banda.name}, ${banda.year}`);
    }
}

async function agregarBandas(datos, nuevabanda) {
    //Agrega la banda al objeto
    datos.bandas.push(nuevabanda);
    //Convierte el objeo en String
    let cadena = JSON.stringify(datos);
    console.log(cadena);
    //Escribe la cadena en el archivo
    fs.writeFileSync(ruta, cadena);
    return datos;
}

async function eliminarBandas(bandas, nombreBanda){
    delete bandas.nombreBanda;
    console.log(bandas);
    return bandas;
}

async function main() {
    console.log("Leyendo datos... ")
    let bandas = await cargarBandas();
    
    //let nuevabanda ={
        //name: "Metallica",
        //year: 1981,
    //};
    //bandas = await agregarBandas(bandas, nuevabanda)
    //mostrarBandas(bandas);

    let nombreBanda = "Queen";
    await eliminarBandas(bandas, nombreBanda);
}

main();
